#!/usr/bin/env groovy

package com.example

class Docker implements Serializable {
    def script

    Docker(script) {
        this.script = script;
    }

    def testApp() {
        script.echo "testing application...."
        script.sh 'mvn test'
    }

    def buildJar() {
        script.echo "building the application..."
        script.sh 'mvn package'
    }

    def buildDockerImage(String imageName) {
        script.echo "building the docker image: $imageName..."
        script.withCredentials([script.usernamePassword(credentialsId:'nexus-credentials', passwordVariable:'PASS',
                usernameVariable: 'USER')]) {
            script.sh "docker build -t $imageName ."
        }
    }

    def loginToNexus() {
        script.echo "logging into Nexus OSS..."
        script.withCredentials([script.usernamePassword(credentialsId:'nexus-credentials', passwordVariable:'PASS',
                usernameVariable: 'USER')]) {
            script.sh "echo $script.PASS | docker login -u $script.USER --password-stdin 143.198.166.147:8083"
        }
    }

    def deployDockerImage(String imageName) {
        script.echo "deploying the docker image: $imageName..."
        script.withCredentials([script.usernamePassword(credentialsId:'nexus-credentials', passwordVariable:'PASS',
                usernameVariable: 'USER')]) {
            script.sh "docker push $imageName"
        }
    }
}